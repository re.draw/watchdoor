import time
import wifi
from machine import Pin, Timer

wlan = wifi.connect()

armado = False
alerta = False
timer = Timer(0)

# prender builtin LED
Pin(2, Pin.OUT, 0)

led = Pin(0, Pin.OUT, 0)
switch = Pin(4, Pin.IN, Pin.PULL_UP)
puerta = Pin(13, Pin.IN, Pin.PULL_UP)
siren = Pin(15, Pin.OUT, 0)

def chequear_puerta_abierta(pin):
    print('[irq] PUERTA')
    def cb(timer):
        global alerta
        alerta = puerta.value() == 1
        print('[alerta -> %s]' % alerta)
    timer.init(mode=Timer.ONE_SHOT, period=1000, callback=cb)

def armar(pin):
    print('[irq] SWITCH')
    def cb(timer):
        global armado
        armado = not armado
        print('[armado -> %s]' % armado)
    timer.init(mode=Timer.ONE_SHOT, period=500, callback=cb)

def enviar_alerta():
    import urequests
    url = "https://maker.ifttt.com/trigger/puerta_lu/with/key/Ck9tQMTPjY9_vM_LdzQEL"
    try:
        print('[webhook] %s' % url)
        urequests.post(url)
    except Exception as e:
        print(e)

def status():
    print('armado={} ALERTA={}'.format(armado, alerta))
    print('switch={} puerta={} led={} siren={}'.format(switch.value(), puerta.value(), led.value(), siren.value()))
    print('***')

switch.irq(armar, Pin.IRQ_FALLING)
puerta.irq(chequear_puerta_abierta, Pin.IRQ_RISING)

while True:
    led.value(armado)

    if armado and alerta:
        siren.value(alerta)

        if wlan.isconnected():
            enviar_alerta()

        timeout = 2 * 60 # minutos

        while armado and timeout > 0:
            print('esperando desarme...')
            timeout -= 1
            # blink
            led.value(not led.value())
            time.sleep(1)
        
        alerta = False

    status()
    time.sleep(0.5)